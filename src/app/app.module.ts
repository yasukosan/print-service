import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import Route Component
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MainComponent } from './main/main.component';
import { BusinessCardComponent } from './business_card/business_card.component';
import { PostcardComponent } from './postcard/postcard.component';
import { RecuruiteComponent } from './recruite/recruite.component';
import { TacksheetComponent } from './tacksheet/tacksheet.component';
import { TacksheetStatusService } from './tacksheet/tacksheet-status.service';
import { LayoutComponent } from './layout/layout.component';
import { LayoutStatusService } from './layout/layout-status.service';
import { PrintComponent } from './print/print.component';
import { PrintStatusService } from './print/print-status.service';

// Import Service
import { LavelSheetService, PostcardService} from './service';
import { PrintDataService, PrintData, PrintText} from './service';
import { PostcardStatusService } from './service';
import { SubjectsService } from './service';

// Import Componente Liblary
import { AlertComponent, LoadingComponent } from './_lib_component';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { AglibModule } from './_lib_service/aglib-module';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    BusinessCardComponent,
    PostcardComponent,
    RecuruiteComponent,
    TacksheetComponent,
    LayoutComponent,
    PrintComponent,
    AlertComponent, LoadingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AglibModule,
    ServiceWorkerModule.register(
      'ngsw-worker.js',
      {
        enabled: environment.production
      }
    ),
  ],
  providers: [
    LavelSheetService, PostcardService,
    TacksheetStatusService, PostcardStatusService,
    LayoutStatusService, PrintStatusService,
    PrintDataService, PrintData, PrintText,
    SubjectsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

