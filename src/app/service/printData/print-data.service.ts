import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';

import { PrintData } from './printData';
import { PrintText } from './printText';
import { HttpService } from '../../_lib_service';

@Injectable()

export class PrintDataService {

    private debug = true;
    private serverUrl = 'server/aj';
    private headerJson: HttpHeaders = new HttpHeaders({
                'Content-Type': 'application/json'
            });

    private responseStatus: any = [];
    private printData: PrintData[] = new Array();
    private printText: PrintText[] = new Array();

    constructor(
        private httpService: HttpService
    ) {}

    /**
     * レイアウトデータを全件取得
     * 主にテンプレートの取得
     * @return Promise<PrintData[]> 印刷データ配列
     */
    public async getAllPrintData(): Promise<PrintData[]> {
        const data = {'job' : '', 'id' : ''};
        data['job'] = 'get_p';
        data['id'] = 'all';

        return this.httpService
                .setServerURL(this.serverUrl)
                .post('', data);
    }

    /**
     * インデックス番号からレイアウトデータを取得
     * @param id number 印刷データの番号
     * @return Promise<PrintData> 印刷データ
     */
    getPrintData(id): Promise<PrintData> {
        if (this.printData[id] === undefined) {
            return this.getAllPrintData()
                .then(print => {
                    return print[id];
                });
        } else {
            return new Promise((resolve, reject) => {
                resolve(this.printData[id]);
            });
        }
    }

    /**
     * テンプレートIDから設定されている文字列を取得
     * @param id number テンプレートのIndex番号
     * @return Promise<PrintText[]> 文字列の配列
     */
    getText(id: number): Promise<PrintText[]> {
        const data = {};
        data['job'] = 'get_t';
        data['print_id'] = Number(id);

        return this.httpService
                .setServerURL(this.serverUrl)
                .post('', data);
    }
    /**
     * 印刷テンプレートの登録
     * @param print PrintData
     * @return Promise<PrintData>
     */
    public setPrint(print: PrintData): Promise<PrintData> {

        const addData: PrintData = print;
        addData['job'] = 'add_p';

        return this.httpService
                .setServerURL(this.serverUrl)
                .setJSONHeader()
                .post('', addData);
    }
    public setText(text: PrintText[]): Promise<any> {

        const addData = {'data': text, 'job': 'add_t'};

        return this.httpService
                .setServerURL(this.serverUrl)
                .setJSONHeader()
                .post('', addData);
    }
    setTextMulti(text: PrintText[], id: number): Promise<any> {
        for (const key in text) {
            if (text.hasOwnProperty(key)) {
                text[key]['print_id'] = Number(id);
            }
        }
        const addData = {'data': text, 'job': 'add_t'};

        return this.httpService
                .setServerURL(this.serverUrl)
                .setJSONHeader()
                .post('', addData);
    }
    updatePrint(print: PrintData): Promise<PrintData> {

        const updata: PrintData = print;
        updata['job'] = 'update_p';

        return this.httpService
            .setServerURL(this.serverUrl)
            .setJSONHeader()
            .post('', updata)
            .then((response) => {
                this.printData[print.id] = response as PrintData;
                return this.printData[print.id];
            });
    }

    updateText(text: PrintText[], id: number): Promise<PrintText[]> {
        for (const key in text) {
            if (text.hasOwnProperty(key)) {
                if (!text[key].hasOwnProperty('print_id')) {
                    text[key]['print_id'] = Number(id);
                }
            }
        }
        const updata = {'data': text, 'job': 'update_t'};
        return this.httpService
            .setServerURL(this.serverUrl)
            .setJSONHeader()
            .post('', updata)
            .then((response) => {
                this.printText = response as PrintText[];
                return this.printText;
            });
    }

    deletePrint(id: number): Promise<void> {

        const del = {'data': id, 'job': 'delete_p'};

        return this.httpService
            .setServerURL(this.serverUrl)
            .setJSONHeader()
            .post('', del);
    }
    deleteText(id: number): Promise<PrintText[]> {

        const del = {'data': id, 'job': 'delete_t'};
        return this.httpService
            .setServerURL(this.serverUrl)
            .setJSONHeader()
            .post('', del);
    }

}
