import { Injectable } from '@angular/core';

@Injectable()
export class TacksheetStatusService {

    flags = {
        onDrag: false,
        onType: true,
        onSheetSpec: false,
        onSheetDesine: false,
        onPrintPosition: false,
        onLoadCSV: false,
        onIncludeCSV: false,
            onDragCSVFile: true,
            onPreviewCSVFile: false,
        onInputContents: false,
        onReviewCSV: false,
        onDownload: false,
        onLoad: false,
        onFileLoad: false,
            onFiles: true,
            onReview: true
    };

    sheetStatus = {
        pageMarginTop: 0,
        pageMarginLeft: 0,
        LabelWidth: 0,
        LabelHeight: 0,
        LabelMarginTop: 0,
        LabelMarginLeft: 0
    };
    printSheetDesine = '';
    printStartPosition = 0;
    printCellCount = 1;
    formStatus = {
        line1: '',
        line2: '',
        line3: '',
        line4: '',
        line5: '',
        postcode: '',
        address: '',
        building: '',
        company: '',
        name: '',
    };
    inputForm = {
        line1: false,
        line2: false,
        line3: false,
        line4: false,
        line5: false,
        postcode: false,
        address: false,
        building: false,
        company: false,
        name: false
    };

    constructor() {}
    /**
     * タックシールの寸法を決定
     * serviceに登録済みのデータを注入するのみで
     * カスタムされた値は自動的にバインディングされる
     * @param data 各寸法
     */
    setupSheetSpec(data: object): void {
        this.sheetStatus.pageMarginTop      = Number(data[0]);
        this.sheetStatus.pageMarginLeft     = Number(data[1]);
        this.sheetStatus.LabelWidth         = Number(data[2]);
        this.sheetStatus.LabelHeight        = Number(data[3]);
        this.sheetStatus.LabelMarginTop     = Number(data[4]);
        this.sheetStatus.LabelMarginLeft    = Number(data[5]);
    }

    /**
     * ウインドウにステータスを設定
     * nullの場合ステータスを反転
     * @param window ウインドウ名
     * @param state 設定ステータス
     */
    public setFlags(window: string, state: boolean = null): void {
        if (state === null) {
            this.flags[window] = (this.flags[window]) ? false : true;
        } else {
            this.flags[window] = state;
        }
    }

    /**
     * ウインドウのフラグ情報を取得
     * @param window string ウインドウ名
     * @return boolean
     */
    public CheckFlags(window: string = null): boolean {
        if (window in this.flags) {
            return this.flags[window];
        } else {
            return false;
        }
    }
    /**
     * 画面切り替え
     * @param window 移動先
     */
    public ChangeWindowStatus(window: string): void {
        for (const key in this.flags) {
            if (this.flags.hasOwnProperty(key)) {
                this.flags[key] = false;
            }
        }
        if (window === 'type') {
            this.flags.onType = true;
        } else if (window === 'spec') {
            this.flags.onSheetSpec = true;
        } else if (window === 'desine') {
            this.flags.onSheetDesine = true;
        } else if (window === 'position') {
            this.flags.onPrintPosition = true;
        } else if (window === 'loadcsv') {
            this.flags.onLoadCSV = true;
        } else if (window === 'includecsv') {
            this.flags.onIncludeCSV = true;
            this.flags.onDragCSVFile = true;
        } else if (window === 'input') {
            this.flags.onInputContents = true;
        } else if (window === 'reviewcsv') {
            this.flags.onReviewCSV = true;
        } else if (window === 'download') {
            this.flags.onDownload = true;
        }
    }
}
