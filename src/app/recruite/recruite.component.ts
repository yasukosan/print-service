import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
// Import TherdParty Service
import { PdfMakerService, RecruiteMakerService, DragService } from '../_lib_service';
// import { ListLayoutService, RecruiteLayoutService } from '../_lib_service';
import { ImageMakerService, ImageOrientationService } from '../_lib_service';

// Import Layout
import { RecruiteLayoutType1Service, RecruiteLayoutType2Service } from '../_lib_service';
import { RecruiteLayoutType3Service, RecruiteLayoutType4Service } from '../_lib_service';
import { RecruiteLayoutType5Service } from '../_lib_service';

// Page Flags
import { RecuruiteFlagsService } from './recruite_flags.service';

@Component({
    selector: 'recruite-print',
    templateUrl: './recruite.component.html',
    styleUrls: ['./recruite.scss']
})

export class RecuruiteComponent {

    reader;
    catOn = false;
    pdfOn = false;

    pageLayout = [];

    fname = '';
    name = '';

    onImage = '';
    onImageType;
    moveSwitch = false;
    mouseStartingPointX = 0;
    mouseStartingPointY = 0;
    mouseMoveCheckX = 0;
    mouseMoveCheckY = 0;
    mouseMoveBaseX = 0;
    mouseMoveBaseY = 0;
    mouseMoveX = 0;
    mouseMoveY = 0;

    transformSwitch = false;
    mouseTranseW = 0;
    mouseTranseH = 0;

    x = 0;
    y = 0;
    width = 200;
    height = 300;

    editTarget = 0;
    editTargetWidth = 0;
    editTargetHeight = 0;
    editName = '';
    canvasBase: HTMLCanvasElement;
    canvasImage: HTMLImageElement;
    canvasWidth;
    canvasHeight;
    canvasRatio;

    catW = 0;
    catH = 0;
    catA = 0;

    subscription: Subscription;
    constructor(
        private pdfmakerService: PdfMakerService,
        // private listlayoutService: ListLayoutService,
        // private recruitelayoutService: RecruiteLayoutService,
        private dragService: DragService,
        private imageOrientationService: ImageOrientationService,
        private imageMakerService: ImageMakerService,
        private recruiteMakerService: RecruiteMakerService,
        private recruiteLayout1: RecruiteLayoutType1Service,
        private recruiteLayout2: RecruiteLayoutType2Service,
        private recruiteLayout3: RecruiteLayoutType3Service,
        private recruiteLayout4: RecruiteLayoutType4Service,
        private recruiteLayout5: RecruiteLayoutType5Service,
        // テンプレート側でURLのエスケープ処理をスルーするのに使っている
        public sanitizer: DomSanitizer,
        private flags: RecuruiteFlagsService,
    ) {
        this.pageLayout[0] = this.recruiteLayout5.getLayoutPage1();
        this.pageLayout[1] = this.recruiteLayout5.getLayoutPage2();
    }

    /**
     * ページの表示可否判定
     * @param page string ページ名
     */
    public chkFlag(page): boolean {
        return this.flags.checkFlags(page);
    }

    public onDragOverHandler(event: DragEvent): void {
        event.preventDefault();
        this.flags.setFlags('onDrag', true);
    }

    public onDragLeaveHandler(event: DragEvent): void {
        event.stopPropagation();
        this.flags.setFlags('onDrag', false);
    }

    /**
     * 読み込んだファイルの表示
     * @param event ドラッグされたファイル
     */
    public onSelectHandler(event, type): void {
        event.preventDefault();
        this.flags
            .setFlags('onPicuture', false)
            .setFlags('onReview', true)
            .setFlags('onDrag', false);
        this.reset('fast');
        if (this.canvasImage) {
            // イベントが残っている場合イベントも初期化
            this.reset('last');
        }
        let files;
        if (type === 'drag') {
            files = event.dataTransfer.files;
        } else if (type === 'select') {
            files = event.target.files;
        }

        // データタイプの判定
        if (!files[0] || files[0].type.indexOf('image/') < 0) {
        } else {
            this.reader = new FileReader();
            this.reader.onloadend = (e) => {
                this.getFileType(e.target.result);
                this.onImage = e.target.result;
                const orientation = this.imageOrientationService.getOrientation(this.onImage);
                console.log(orientation);
                if (orientation === 0 || orientation === 1) {
                    this.resultImage();
                } else {
                    this.rotationImage(orientation);
                }
                event.stopPropagation();
            };
            this.reader.readAsDataURL(files[0]);
        }
    }
    /**
     * 読み込まれた画像の表示
     * @param event マウスイベント
     */
    public resultImage(): void {
        this.dragService.setImage(this.onImage);
        this.dragService.resultImage('face-shot')
            .then((result) => {
                this.canvasImage = result;
            });
    }
    public rotationImage(rotate): void {

        this.imageOrientationService
            .ImgRotation(this.onImage, rotate)
            .then((img) => {
                this.onImage = img;
                this.resultImage();
            });

    }

    /**
     * 画像のトリミング
     * リサイズ処理を行っているので
     * 切り取り位置の指定は縮小率を考慮する必要あり
     */
    public catImage(): void {
        const oc = <HTMLCanvasElement> document.createElement('canvas');
        const octx = oc.getContext('2d');

        const img = new Image();

        img.onload = (e) => {
            oc.setAttribute('width', (this.catW / this.canvasRatio).toString());
            oc.setAttribute('height', (this.catH / this.canvasRatio).toString());
            octx.drawImage(
                img, // 切り取りイメージ
                (this.mouseStartingPointX / this.canvasRatio), // 切り取り開始X座標
                (this.mouseStartingPointY / this.canvasRatio), // 切り取り開始Y座標
                this.catW / this.canvasRatio, // 切り取り幅
                this.catH / this.canvasRatio, // 切り取り高さ
                0, 0,
                this.catW / this.canvasRatio,
                this.catH / this.canvasRatio); // 切り取り後の表示位置とサイズ

            // 切り取り後のイメージを保存
            this.onImage = oc.toDataURL(this.onImageType);
            this.catOn = false;
            this.reset('last');

            this.resultImage();
        };
        img.src = this.onImage;
    }

/******************************************************
 * ページ毎の処理
 *
 *****************************************************/

    public setPicutureType(choice: boolean): void {
        if (choice) {
            this.flags.moveWindow('edit');
        } else {
            this.flags.moveWindow('download');
        }

    }
    public setPicutureEdit(choice: boolean): void {
        this.flags.moveWindow('edit');

    }
    public setReview(): void {
        if (!this.pageLayout.hasOwnProperty(0)) {
            this.setLayout(0);
        }
        this.flags.moveWindow('download');
    }
    public setDownload(): void {
        this.buildIMage();
    }

    public reset(type: string): void {
        if (type === 'last') {
            this.dragService.reset('last');
        } else if (type === 'first') {
            this.reader = null;
            this.canvasImage = null;
            this.canvasBase = null;
            this.canvasImage = null;
            this.mouseMoveCheckX = null;
            this.mouseMoveCheckY = null;
            this.catOn = false;
            this.moveSwitch = false;
        }
    }
    public setupLayoutType(type: number): void {
        this.pageLayout = this.setLayout(type);
    }

    public setLayout(type: number): string[] {
        if (type === 1) {
            return [
                this.recruiteLayout1.getLayoutPage1(),
                this.recruiteLayout1.getLayoutPage2()
            ];
        } else if (type === 2) {
            return [
                this.recruiteLayout1.getLayoutPage1(),
                this.recruiteLayout2.getLayoutPage2()
            ];
        } else if (type === 3) {
            return [
                this.recruiteLayout1.getLayoutPage1(),
                this.recruiteLayout3.getLayoutPage2()
            ];
        } else if (type === 4) {
            return [
                this.recruiteLayout1.getLayoutPage1(),
                this.recruiteLayout4.getLayoutPage2()
            ];
        } else if (type === 0) {
            return [
                this.recruiteLayout5.getLayoutPage1(),
                this.recruiteLayout5.getLayoutPage2()
            ];
        }
    }

    public buildIMage(): void {
        this.imageMakerService.setResulution(13.78095);
        this.imageMakerService.setSheetSize(210, 297);
        this.imageMakerService.setSheetBackground(this.pageLayout[0]);
        this.imageMakerService.overFaceShot(this.onImage).then(
            (img1) => {
                const image0 = img1;
                this.imageMakerService.setSheetBackground(this.pageLayout[1]);
                this.imageMakerService.svgToImg().then(
                    (img2) => {
                        this.pageLayout = [image0, img2];
                        this.buildPdf();
                    }
                );
            }
        );
    }

    public buildPdf(): void {
        this.recruiteMakerService.setResolution(2.8333);
        this.recruiteMakerService.setLayout(this.pageLayout);

        const layout = this.recruiteMakerService.makeRecruiteLayout();
        const userAgent = window.navigator.userAgent.toLowerCase();
        this.pdfmakerService.pdfMakeForIE(layout);
    }

    public getFileType(file): void {
        const header = file.split(';');
        const type = header[0].split(':');
        this.onImageType = type[1];
    }
}
