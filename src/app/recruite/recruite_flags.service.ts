
export class RecuruiteFlagsService {

    private flags = {
        onFileLoad: false,
            onPicuture: true,
            onReview: false,
            onFaceCat: false,
        onDrag: false,
        onType: false,
        onSelectPhoto: true,
        onDownload: false,
        onLoad: false
    };

    constructor() {}

    /**
     * ページフラグを取得
     * @param page string ページ名
     * @return boolean
     */
    public checkFlags(page): boolean {
        if (page in this.flags) {
            return this.flags[page]
        } else {
            return false;
        }
    }

    /**
     * フラグを設定
     * @param page string ページ名
     * @param flag boolean 設定フラグ
     * @return RecuruiteFlagsService
     */
    public setFlags(page: string, flag: boolean = false): RecuruiteFlagsService {
        if (page in this.flags) {
            this.flags[page] = flag;
            return this;
        } else {
            return this;
        }
    }

    /**
     * ページ移動
     * @param window string 移動先ページ名
     */
    public moveWindow(window: string): void {
        for (const key in this.flags) {
            if (this.flags.hasOwnProperty(key)) {
                this.flags[key] = false;
            }
        }
        if (window === 'type') {
            this.flags.onType = true;
            this.flags.onPicuture = true;
        } else if (window === 'picture') {
            this.flags.onSelectPhoto = true;
        } else if (window === 'edit') {
            this.flags.onFileLoad = true;
            this.flags.onPicuture = true;
        } else if (window === 'download') {
            this.flags.onDownload = true;
        }
    }

}
